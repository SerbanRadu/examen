package Categories;
import Entities.Product;


public abstract class Category {
    int id;
    String name;


    public Category(int id, String name){
        this.id=id;
        this.name=name;
    }

    public Category() {

    }

    public void addProduct(Product p){
    }

    public void removeProduct(Product p){

    }

    public void updateProduct(Product p){

    }
    public Product getProduct(int id){
        return new Product() {};
    }
}
