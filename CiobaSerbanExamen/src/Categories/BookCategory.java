package Categories;

import Entities.Book;
import Entities.Product;

import java.util.ArrayList;

    public class BookCategory extends Category {

        ArrayList<Book> list_of_books;

        public BookCategory() {
            super();
            this.list_of_books=new ArrayList<>();
        }

        public BookCategory(int id, String name){
            super(id,name);
            this.list_of_books=new ArrayList<>();
        }

        public void addProduct(Book p){
            this.list_of_books.add(p);
        }

        public void removeProduct(Book p){
            this.list_of_books.remove(p);
        }

        public void updateProduct(int i, Book b){
            this.list_of_books.get(i).setCategoryId(b.getCategoryId());
            this.list_of_books.get(i).setId(b.getId());
            this.list_of_books.get(i).setName(b.getName());
            this.list_of_books.get(i).setPrice(b.getPrice());
        }
        public Book getProduct(int id){
            Book book = null;
            for(Book b: this.list_of_books){
                if (b.getId() == id){
                    book= b;
                }
            }
            return book;
        }

        @Override
        public String toString() {
            return "BookCategory{" +
                    "list_of_books=" + list_of_books +
                    '}';
        }
    }

