package main;

import java.util.Scanner;
import Categories.*;
import Entities.*;
public class main {

    public static void printMenu(){
        System.out.println("1 - adaugare produs intr-o categorie");
        System.out.println("2 - stergere produs dintr-o categorie");
        System.out.println("3 - editarea unui produs existent");
        System.out.println("4 - punerea unui produs la vanzare");
        System.out.println("5 - retragerea unui produs de la vanzare");
    }

    public static void main(String[] args) {
        BookCategory bookCategory = new BookCategory();
        printMenu();
        while(true){
            Scanner in = new Scanner(System.in);

            String s = in.nextLine();

            if(s!=null)
                if(s.equals("1")){
                    Book book  = new Book(1,1,"Marvel", 200);
                    bookCategory.addProduct(book);
                    System.out.println(bookCategory.toString());
                }
            printMenu();

        }

    }
}

