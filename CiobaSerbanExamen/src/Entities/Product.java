package Entities;
import Actions.*;


public abstract class Product extends ProductDetails implements RetireProduct, SellProduct{
    boolean forSale;
    int categoryId;

    public Product(){

    }

    public Product(int categoryId, int id, String name, int price ){
        super.id=id;
        super.name=name;
        super.price=price;
        this.categoryId=categoryId;
        this.forSale = false;
    }

    public void markForSale() {
        this.forSale= true;
    }

    public void retireFromSale(){
        this.forSale= false;
    }

    public boolean getForSale() {
        return forSale;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return id + "," + name + "," + price + "," + categoryId + "," + forSale;
    }

}
