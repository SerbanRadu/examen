package Entities;

public class Toy extends Product{
    public Toy(int categoryId, int id, String name, int price) {
        super(categoryId, id, name, price);
        super.forSale = false;
    }

    public void markForSale() {
        super.forSale= true;
    }

    public void retireFromSale(){
        super.forSale= false;
    }
}
