package Entities;

public class Book extends Product{

    public Book(int categoryId, int id, String name, int price) {
        super(categoryId, id, name, price);
        super.forSale = false;
    }

    public void markForSale() {
        super.forSale= true;
    }

    public void retireFromSale(){
        super.forSale= false;
    }

}
